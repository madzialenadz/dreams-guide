package api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Main {

    @SerializedName("temp")
    @Expose
    public Float temp;
    @SerializedName("humidity")
    @Expose
    public Long humidity;
    @SerializedName("pressure")
    @Expose
    public Float pressure;
    @SerializedName("temp_min")
    @Expose
    public Float tempMin;
    @SerializedName("temp_max")
    @Expose
    public Float tempMax;

    public Float getTemp() {
        return temp;
    }

    public Long getHumidity() {
        return humidity;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getTempMin() {
        return tempMin;
    }

    public Float getTempMax() {
        return tempMax;
    }
}
