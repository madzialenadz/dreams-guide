package magdalena.pl.dreamguide;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharePreference {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static final String PREFS_NAME = "USER_PREFS";
    private static final String EMAIL = "USER_EMAIL";
    private static final String SOCIAL_LINK = "SOCIAL_LINK";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String NAME = "NAME";
    private static final String SET_TIME_INCENTIVE = "SET_TIME_INCENTIVE";
    private static final String FIREBASE_ID = "FIREBASE_ID";
    private static final String PIN_CODE = "PIN_CODE";

    public MySharePreference() {
    }

    public void clearEmailUser(Context context) {
        sharedPreferences = context.getSharedPreferences(EMAIL, 0);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void clearFirebaseId(Context context) {
        sharedPreferences = context.getSharedPreferences(FIREBASE_ID, 0);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void clearSocialLink(Context context) {
        sharedPreferences = context.getSharedPreferences(SOCIAL_LINK, 0);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void saveEmailUser(Context context, String email) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public void saveFirebaseId(Context context, String firebaseId) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(FIREBASE_ID, firebaseId);
        editor.apply();
    }

    public void saveSocialLink(Context context, String socialLink) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(SOCIAL_LINK, socialLink);
        editor.apply();
    }

    public void saveLanguage(Context context, String lang) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(LANGUAGE, lang);
        editor.apply();
    }

    public void saveName(Context context, String name) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(NAME, name);
        editor.apply();
    }

    public void saveTimeIncetive(Context context, String time) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(SET_TIME_INCENTIVE, time);
        editor.apply();
    }

    public void savePinCode(Context context, String pinCode){
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putString(PIN_CODE, pinCode);
        editor.apply();
    }

    public String getEmailUser(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFS_NAME, 0);
        String emailString = sharedPref.getString(EMAIL, "");
        return emailString;
    }

    public String getFirebaseId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String firebaseId = sharedPreferences.getString(FIREBASE_ID, "");
        return firebaseId;
    }

    public String getSocialLink(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String socialLink = sharedPreferences.getString(SOCIAL_LINK, "");
        return socialLink;
    }

    public String getLanguage(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String languageString = sharedPreferences.getString(LANGUAGE, "");
        return languageString;
    }

    public String getName(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String nameString = sharedPreferences.getString(NAME, "");
        return nameString;
    }

    public String getPinCode(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String pinCodeString = sharedPreferences.getString(PIN_CODE, "");
        return pinCodeString;
    }

    public String getTimeIncentive(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String timeString = sharedPreferences.getString(SET_TIME_INCENTIVE, "");
        return timeString;
    }


}