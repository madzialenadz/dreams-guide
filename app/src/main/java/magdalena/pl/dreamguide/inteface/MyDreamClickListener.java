package magdalena.pl.dreamguide.inteface;

import android.view.View;

public interface MyDreamClickListener {
    void onClick(View view, int position);
    void showMenu(View view, int position);
}