package magdalena.pl.dreamguide.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import magdalena.pl.dreamguide.fragment.FistSwipeViewFragment;
import magdalena.pl.dreamguide.fragment.SecondSwipeViewFragment;
import magdalena.pl.dreamguide.fragment.ThirdSwipeViewFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new FistSwipeViewFragment();
            case 1:
                return new SecondSwipeViewFragment();
            case 2:
                return new ThirdSwipeViewFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}