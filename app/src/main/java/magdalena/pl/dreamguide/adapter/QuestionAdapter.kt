package magdalena.pl.dreamguide.adapter

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.question_item.view.*
import magdalena.pl.dreamguide.R
import magdalena.pl.dreamguide.model.Question

class QuestionAdapter(private val listQuestion: List<Question>) : RecyclerView.Adapter<QuestionAdapter.QuestionHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.question_item, parent, false)
        return QuestionHolder(v)
    }

    override fun getItemCount(): Int = listQuestion.size

    override fun onBindViewHolder(holder: QuestionHolder, position: Int) {
        holder.bindItems(listQuestion[position])

    }

    class QuestionHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItems(question: Question) {

            itemView.header_text_view.text = question.titleHeader.toString()
            itemView.header_content.text = question.answerQuestion.toString()

            itemView.header_view.setOnClickListener {

                var drawable: Drawable? = null

                if (itemView.header_content.visibility == View.VISIBLE) {
                    itemView.header_content.visibility == View.GONE
                    drawable = itemView.context.getDrawable(R.drawable.ic_expand_more)
                } else {
                    itemView.header_content.visibility == View.VISIBLE
                    drawable = itemView.context.getDrawable(R.drawable.ic_expand_less)
                }
                itemView.header_text_view.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
            }
        }
    }

}


