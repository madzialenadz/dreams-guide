package magdalena.pl.dreamguide.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.model.Dream;

public class SelectDreamAdapter extends RecyclerView.Adapter<SelectDreamAdapter.ViewHolder> {

    private List<Dream> dreamList = new ArrayList<>();
    private Context context;
    int numberCheckboxesChecked = 0;

    public SelectDreamAdapter(List<Dream> dreamList, Context context) {
        this.dreamList = dreamList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_dream, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Dream dream = dreamList.get(position);
        Glide.with(context).load(dream.getImageUrl()).into(holder.imageDream);
        holder.nameDream.setText(dream.getNameDream());
        holder.isSelected.setChecked(dream.isSelected());
        holder.isSelected.setTag(Integer.valueOf(position));
        holder.isSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                int clickedPos = ((Integer) cb.getTag()).intValue();
                if (cb.isChecked() && numberCheckboxesChecked >= 1) {
                    cb.setChecked(false);
                    Toast.makeText(context, R.string.max_allowed_one_symbol_dream_only,Toast.LENGTH_LONG).show();
                } else {
                    if (cb.isChecked()) {
                        numberCheckboxesChecked++;
                    } else {
                        numberCheckboxesChecked--;
                    }
                    dreamList.get(clickedPos).setSelected(cb.isChecked());
                    notifyDataSetChanged();
                }
            }
        });
    }

    public void updateList(List<Dream> dreams) {
        this.dreamList = dreams;
        notifyDataSetChanged();
    }

    public List<Dream> getSelectedItem() {
        List<Dream> dreamModelList = new ArrayList<>();
        for (int i = 0; i < dreamList.size(); i++) {
            Dream dream = dreamList.get(i);
            if (dream.isSelected()) {
                dreamModelList.add(dream);
            }
        }
        return dreamModelList;
    }

    @Override
    public int getItemCount() {
        return dreamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageDream)
        ImageView imageDream;
        @BindView(R.id.nameDream)
        TextView nameDream;
        @BindView(R.id.checkboxDream)
        CheckBox isSelected;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}