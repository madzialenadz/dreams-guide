package magdalena.pl.dreamguide.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.activity.DreamDetailActivity;
import magdalena.pl.dreamguide.model.Dream;

public class DreamAdapter extends RecyclerView.Adapter<DreamAdapter.DreamViewHolder> {


    private List<Dream> dreamList = new ArrayList<>();
    private Context context;

    public DreamAdapter(List<Dream> dreamList, Context context) {
        this.dreamList = dreamList;
        this.context = context;
    }

    @NonNull
    @Override
    public DreamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dream, parent, false);
        return new DreamViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DreamViewHolder holder, int position) {
        Dream dream = dreamList.get(position);
        holder.dreamTextView.setText(dream.getNameDream());
        Glide.with(context).load(dream.getImageUrl()).into(holder.dreamImageView);
    }

    @Override
    public int getItemCount() {
        return dreamList.size();
    }


    public void updateList(List<Dream> dreams) {
        this.dreamList = dreams;
        notifyDataSetChanged();
    }

    public class DreamViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dreamsCardView)
        CardView cardView;
        @BindView(R.id.dreamImageView)
        ImageView dreamImageView;
        @BindView(R.id.dreamsNameTextView)
        TextView dreamTextView;

        DreamViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Dream dream;
            dream = dreamList.get(getAdapterPosition());
            Intent intent = new Intent(context, DreamDetailActivity.class);
            intent.putExtra("dreamImageUrl", dream.getImageUrl());
            intent.putExtra("dreamName", dream.getNameDream());
            intent.putExtra("dreamDetails", dream.getDescriptionDreams());
            context.startActivity(intent);
        }
    }
}