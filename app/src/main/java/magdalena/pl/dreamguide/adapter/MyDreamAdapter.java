package magdalena.pl.dreamguide.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.inteface.MyDreamClickListener;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.model.MyDream;

public class MyDreamAdapter extends RecyclerView.Adapter<MyDreamAdapter.ViewHolder> {


    private List<MyDream> myDreamList = new ArrayList<>();

    public void setMyDreamClickListener(MyDreamClickListener myDreamClickListener) {
        this.myDreamClickListener = myDreamClickListener;
    }

    private MyDreamClickListener myDreamClickListener;

    public MyDreamAdapter(List<MyDream> myDreamList) {
        this.myDreamList = myDreamList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mydream, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        MyDream itemDream = myDreamList.get(position);
        holder.dreamDateTextView.setText(itemDream.getSaveDataDream());
        holder.dreamNameTextView.setText(itemDream.getTitleDream());
        holder.deleteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDreamClickListener.showMenu(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return myDreamList.size();
    }

    public void removeMyDream(int position) {
        myDreamList.remove(position);
        notifyItemRemoved(position);

    }

    public void addMyDream(MyDream mydream) {
        myDreamList.add(mydream);
        notifyDataSetChanged();

    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.deleteDreamButton)
        ImageView deleteImageView;
        @BindView(R.id.dreamCardView)
        CardView dreamCardView;
        @BindView(R.id.dreamNameTextView)
        TextView dreamNameTextView;
        @BindView(R.id.dreamDateTextView)
        TextView dreamDateTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            dreamCardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myDreamClickListener.onClick(v, getAdapterPosition());
        }
    }
}