package magdalena.pl.dreamguide.adapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.model.Emotion;


public class EmotionAdapter extends RecyclerView.Adapter<EmotionAdapter.ViewHolder> {

    private int selectedPosition = -1;
    private List<Emotion> emotionList = new ArrayList<>();

    public EmotionAdapter(List<Emotion> emotionList) {
        this.emotionList = emotionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemDream = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_emotion, parent, false);
        return new ViewHolder(itemDream);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Emotion emotion = emotionList.get(position);
        holder.emotionImageView.setImageResource(emotion.getImageView());
        holder.nameEmotion.setText(emotion.getEmotionName());

        if (selectedPosition == position) {
            holder.backgroundEmotion.setBackgroundResource(R.color.colorSelectedDark);
        } else {
            holder.backgroundEmotion.setBackgroundResource(R.color.colorBackground);
        }
    }

    // pobiera informacje o zaznaczonym snie

    public Emotion getSelectedEmotionItem() {
        return emotionList.get(selectedPosition);
    }

    //zaznacza emocje
    public void setSelectedEmotion(int position) {
        selectedPosition = position;
    }


    public int selectedPosition() {
        return selectedPosition;
    }

    @Override
    public int getItemCount() {
        return emotionList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.backgroundEmotion)
        LinearLayout backgroundEmotion;

        @BindView(R.id.emotionImageView)
        ImageView emotionImageView;

        @BindView(R.id.nameEmotion)
        TextView nameEmotion;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            backgroundEmotion.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            selectedPosition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}