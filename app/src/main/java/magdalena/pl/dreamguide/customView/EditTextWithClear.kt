package magdalena.pl.dreamguide.customView

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import magdalena.pl.dreamguide.R

class EditTextWithClear : AppCompatEditText {

    private var mClearButtonImage: Drawable? = null

    constructor(context: Context) : super(context) {
        setupEditText()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupEditText()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        setupEditText()
    }



    private fun setupEditText() {

        mClearButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.ic_backspace, null)


        setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, mClearButtonImage, null)

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {

                    val length: Int = this.text!!.length
                    if (length > 0) {
                        this.text?.delete(length - 1, length)
                        return@OnTouchListener true
                    }

                }
            }
            return@OnTouchListener false
        })
    }

}