package magdalena.pl.dreamguide.activity;

import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.dialog.SelectedLanguageDialogFragment;
import magdalena.pl.dreamguide.dialog.TimePickerDialogFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.selectedLanguageTextView)
    TextView selectedLanguageTextView;
    @BindView(R.id.selectedLanguageClick)
    ImageView selectedLanguageClick;
    @BindView(R.id.selectedLanguageLayout)
    LinearLayout selectedLanguageLayout;
    @BindView(R.id.languageLayout)
    LinearLayout languageLayout;
    @BindView(R.id.setTimeIncentiveTextView)
    TextView setTimeIncentiveTextView;
    @BindView(R.id.setTimeIncentiveClick)
    ImageView setTimeIncentiveClick;
    @BindView(R.id.setTimeIncentive)
    LinearLayout setTimeIncentive;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        setToolbar();

        setLanguage();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.selectedLanguageClick, R.id.setTimeIncentiveClick})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.selectedLanguageClick:
                FragmentManager fm1 = getFragmentManager();
                SelectedLanguageDialogFragment selectedLanguageDialogFragment = new SelectedLanguageDialogFragment();
                selectedLanguageDialogFragment.show(fm1, "SelectedLanguage");
                break;
            case R.id.setTimeIncentiveClick:
                FragmentManager fm3 = getFragmentManager();
                TimePickerDialogFragment timePickerDialogFragment = new TimePickerDialogFragment();
                timePickerDialogFragment.show(fm3, "Picker");
                break;
        }
    }

    public void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
    }


    public void setLanguage() {

        MySharePreference mySharePreference = new MySharePreference();
        String timeIncentiveString = mySharePreference.getTimeIncentive(SettingActivity.this);
        String languageString = mySharePreference.getLanguage(SettingActivity.this);
        setLocale(languageString);

        if (!languageString.isEmpty()) {
            switch (languageString) {
                case "en":
                    selectedLanguageTextView.setText(R.string.englishLanguage);
                    setLocale("en");
                    break;
                case "pl":
                    selectedLanguageTextView.setText(R.string.polishLanguage);
                    setLocale("pl");
                    break;
            }
        }

        if (timeIncentiveString.isEmpty()) {
            setTimeIncentiveTextView.setText("06:00 AM");
        } else {
            setTimeIncentiveTextView.setText(timeIncentiveString);
        }

    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
