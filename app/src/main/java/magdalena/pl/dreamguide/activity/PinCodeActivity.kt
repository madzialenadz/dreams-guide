package magdalena.pl.dreamguide.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_pin_code.*
import kotlinx.android.synthetic.main.content_pin_code.*
import magdalena.pl.dreamguide.MySharePreference

import magdalena.pl.dreamguide.R

class PinCodeActivity : AppCompatActivity() {


    lateinit var mySharePreference: MySharePreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_code)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)


        val extras = intent.extras
        if (extras != null) {
            val setPinAction = intent.getStringExtra("setPinCodeAction")



            when (setPinAction) {

                "createPinCode" -> {

                    toolbar_text_view_pin_code.setText(R.string.sign_in_with_pin_code_toolabr)
                    text_view_pin_code.setText(R.string.create_a_4_digtal_pin_code)

                }

                "checkPinCode" -> {

                    toolbar_text_view_pin_code.setText(R.string.change_pin_code_toolbar)
                    text_view_pin_code.setText(R.string.enter_the_4_digit_pin)


                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                }


                "updatePinCode" -> {

                    toolbar_text_view_pin_code.setText(R.string.create_pin_code_toolbar)
                    text_view_pin_code.setText(R.string.change_pin_code)

                }
            }
        }
    }


    fun keyClicked(view:View){

      var button =    view as Button

    }

}
