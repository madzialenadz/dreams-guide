package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.dreamTextView)
    TextView dreamTextView;
    @BindView(R.id.guideTextView)
    TextView guideTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        setLanguage();


        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, SwipeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }

        }.execute();
    }

    public void setLanguage() {

        MySharePreference mySharePreference = new MySharePreference();
        String deviceLanguageCurrent = Locale.getDefault().getLanguage();
        String languageString = mySharePreference.getLanguage(SplashScreenActivity.this);

        if (languageString.isEmpty()) {
            mySharePreference.saveLanguage(SplashScreenActivity.this, deviceLanguageCurrent);
            setLocale(deviceLanguageCurrent);
        } else {
            setLocale(languageString);
        }
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}