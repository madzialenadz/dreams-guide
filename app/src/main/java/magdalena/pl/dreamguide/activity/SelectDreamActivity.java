package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.SelectDreamAdapter;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.Dream;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SelectDreamActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.addSymbolDream)
    Button addSymbolDream;
    private List<Dream> dreamList;
    private Dao<Dream, Integer> dreamDao;
    private DbHelper dbHelper;
    StringBuilder stringDreamSymbol;
    private SelectDreamAdapter selectDreamAdapter;
    @BindView(R.id.dreamRecycleView)
    RecyclerView dreamRecycleView;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_dream);

        ButterKnife.bind(this);

        setToolbar();

        setLanguage();

        setDatabase();

        setDreamRecycleView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search_dream);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.search_dream));
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(newText);
        return false;
    }

    void filter(String text) {
        List<Dream> dream = new ArrayList<>();
        for (Dream d : dreamList) {
            if (d.getNameDream().toLowerCase().contains(text)) {
                dream.add(d);
            }
        }
        selectDreamAdapter.updateList(dream);
    }

    @OnClick(R.id.addSymbolDream)
    public void onViewClicked() {
        hideKeyboard();

        List<Dream> list = selectDreamAdapter.getSelectedItem();
        if (list.size() > 0) {
            stringDreamSymbol = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                Dream dream = list.get(i);
                stringDreamSymbol.append(dream.getNameDream() + "");
            }
        }

        if (stringDreamSymbol != null) {
            Intent intent = new Intent();
            intent.putExtra("SYMBOL_DREAM", stringDreamSymbol.toString());
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Toast.makeText(this, R.string.you_did_select_symbol_dream, Toast.LENGTH_SHORT).show();
        }
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
    }

    public void setLanguage() {
        MySharePreference sharePreference = new MySharePreference();

        String languageString = sharePreference.getLanguage(SelectDreamActivity.this);
        setLocale(languageString);

    }

    public void setDatabase() {

        dbHelper = new DbHelper(this);

        try {
            dreamDao = dbHelper.getDao(Dream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            dreamList = dreamDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setDreamRecycleView() {

        if (dreamList != null && dreamList.size() > 0) {
            selectDreamAdapter = new SelectDreamAdapter(dreamList, SelectDreamActivity.this);
            linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            dreamRecycleView.setLayoutManager(linearLayoutManager);
            dreamRecycleView.setAdapter(selectDreamAdapter);
        } else {
            Toast.makeText(this, R.string.list_dream, Toast.LENGTH_SHORT).show();
        }

    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
