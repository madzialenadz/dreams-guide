package magdalena.pl.dreamguide.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import api.model.Main;
import api.model.Weather;
import api.model.WeatherResponse;
import api.model.Wind;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.app.Application;
import magdalena.pl.dreamguide.dialog.ErrorDialogNetwork;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoodMorningActivity extends AppCompatActivity {

    public static final String TAG = GoodMorningActivity.class.getCanonicalName();
    static final int REQUEST_LOCATION = 1;

    @BindView(R.id.currentDataPlaceTextView)
    TextView currentDataPlaceTextView;
    @BindView(R.id.temperatureTextView)
    TextView temperatureTextView;
    @BindView(R.id.dataTemperatureTextView)
    TextView dataTemperatureTextView;
    @BindView(R.id.pressureTextView)
    TextView pressureTextView;
    @BindView(R.id.dataPressureTextView)
    TextView dataPressureTextView;
    @BindView(R.id.speedWindTextView)
    TextView speedWindTextView;
    @BindView(R.id.dataSpeedWindTextView)
    TextView dataSpeedWindTextView;
    @BindView(R.id.weatherImageView)
    ImageView weatherImageView;
    @BindView(R.id.nameUserTextView)
    TextView nameUserTextView;
    @BindView(R.id.addDreamTextView)
    TextView addDreamTextView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_morning);
        ButterKnife.bind(this);

        swipeRefresh.setEnabled(false);

        MySharePreference mySharePreference = new MySharePreference();
        String nameUser = mySharePreference.getName(GoodMorningActivity.this);
        if (nameUser != null) {
            nameUserTextView.setText(nameUser);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }


    @Override
    protected void onResume() {
        super.onResume();

       // getWeatherLocation("53.12754311", "23.0159837");


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                String latitude = String.valueOf(location.getLatitude());
                String longitude =String.valueOf(location.getLongitude());

                getWeatherLocation(latitude,longitude);
            }

        }
    }

    @OnClick(R.id.addDreamTextView)
    public void onViewClicked() {
        Intent addDreamIntent = new Intent();
        addDreamIntent.setClass(GoodMorningActivity.this, MyDreamActivity.class);
        startActivity(addDreamIntent);
        finish();
    }

    private void getWeatherLocation(String latitude, String longitude) {

        if (swipeRefresh != null && !swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(true);
        }

        Application.weatherApi.getWeather(Application.API_KEY, latitude, longitude).enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                Log.d(TAG, "onResponse:");

                if (swipeRefresh != null && !swipeRefresh.isRefreshing()) {
                    swipeRefresh.setRefreshing(false);
                }

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().weather != null && response.body().weather.size() > 0) {

                            Weather weather = response.body().weather.get(0);
                            Wind wind = response.body().wind;
                            Main main = response.body().main;

                            currentDataPlaceTextView.setText(getCurrentDate() + " " + response.body().name);
                            dataTemperatureTextView.setText(convertKelvinCelcius(main.getTemp()));
                            dataPressureTextView.setText(String.format("%s hPa", Float.toString(main.getPressure())));
                            dataSpeedWindTextView.setText(String.format("%s m/s", Float.toString(wind.getSpeed())));

                            Glide.with(GoodMorningActivity.this)
                                    .load("http://api.openweathermap.org/img/w/" + weather.icon + ".png")
                                    .into(weatherImageView);

                        }
                    }
                }
            }

            @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.d(TAG, "onFailure:");

                if (swipeRefresh != null && !swipeRefresh.isRefreshing()) {
                    swipeRefresh.setRefreshing(false);
                }

                FragmentManager fm = getFragmentManager();
                ErrorDialogNetwork errorDialogNetwork = new ErrorDialogNetwork();
                errorDialogNetwork.show(fm, "abc");
            }
        });

    }


    private String convertKelvinCelcius(Float Kelvin) {
        Float Celsius = Kelvin - 273.15f;
        return Float.toString(Celsius);
    }

    private String getCurrentDate() {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("EEEE dd.MM.yyyy");
        String dayOfWeek = ft.format(date);
        String changeDayOfWeek = dayOfWeek.replace(dayOfWeek.substring(0, 1), dayOfWeek.substring(0, 1).toUpperCase());
        return changeDayOfWeek;
    }

}
