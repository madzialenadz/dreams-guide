package magdalena.pl.dreamguide.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.app.Application;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {

    ProgressDialog progress;
    private static final int RESULT_LOAD_IMAGE = 2;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private Dao<User, ?> dao;
    private DbHelper dbHelper;
    private User userMy;
    Uri photoUri, pickedImage;
    String userFirebaseId, nameString, surnameString, fullNameString, photoString;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.myPictureImageView)
    ImageView myPictureImageView;
    @BindView(R.id.saveProfileButton)
    Button saveProfileButton;
    @BindView(R.id.changeImageButton)
    TextView changeImageButton;
    @BindView(R.id.NameEditText)
    TextInputEditText nameEditText;
    @BindView(R.id.inputNameEditText)
    TextInputLayout inputNameEditText;
    @BindView(R.id.SurnameEditText)
    TextInputEditText surnameEditText;
    @BindView(R.id.inputSurnameEditText)
    TextInputLayout inputSurnameEditText;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_profile);
        setToolbar();
        ButterKnife.bind(this);
        setSettingPreferences();
        authFirebaseSet();
        setDataUserDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle(R.string.title_alert_dialog_delete);
                alertDialog.setMessage(R.string.message_alert_dialog_delete);
                alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        if (user != null) {
                            progress = ProgressDialog.show(ProfileActivity.this, "", getString(R.string.delete_user), true);

                            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        DeleteBuilder<User, ?> deleteBuilder = dao.deleteBuilder();
                                        try {
                                            deleteBuilder.where().eq(User.Columns.FIREBASE_ID, userFirebaseId);
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            deleteBuilder.delete();
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }

                                        Toast.makeText(ProfileActivity.this, R.string.text_delete_success, Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        ProfileActivity.this.finish();
                                    } else {
                                        Toast.makeText(ProfileActivity.this, R.string.text_delete_error, Toast.LENGTH_SHORT).show();
                                    }
                                    dialog.dismiss();
                                }
                            });
                        }

                    }
                });
                alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.create();
                alertDialog.show();
                break;
            case R.id.action_editEmail:

                final UpdateBuilder<User, ?> updateUser = dao.updateBuilder();
                try {
                    updateUser.where().eq(User.Columns.FIREBASE_ID, userFirebaseId);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                dialogBuilder.setTitle(R.string.edit_email_dialog);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.edit_email, null);
                dialogBuilder.setView(dialogView);

                final EditText editText = (EditText) dialogView.findViewById(R.id.emailEditText);
                editText.setText(userMy.getEmail());
                dialogBuilder.setMessage(R.string.edit_email_dialog_message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        boolean isError = false;

                        final String email = editText.getText().toString();

                        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                            editText.setError(getString(R.string.this_email_is_incorrect));
                            editText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"));
                            isError = true;
                        }

                        if (email.isEmpty()) {
                            editText.setError(getString(R.string.this_field_is_required));
                            editText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"));
                            isError = true;
                        }
                        if (!isError) {
                            progress = ProgressDialog.show(ProfileActivity.this, "", getString(R.string.text_update_email), true);

                            user.updateEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        try {
                                            updateUser.updateColumnValue(User.Columns.EMAIL, email);
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            updateUser.update();
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }

                                        Toast.makeText(ProfileActivity.this, R.string.your_email, Toast.LENGTH_SHORT).show();
                                        progress.dismiss();
                                    } else {

                                        progress.dismiss();
                                        Toast.makeText(ProfileActivity.this, R.string.text_email_error, Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                        }

                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogBuilder.create();
                dialogBuilder.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.changeImageButton, R.id.saveProfileButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.changeImageButton:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
                break;
            case R.id.saveProfileButton:

                boolean isError = false;

                String updateName = nameEditText.getText().toString();
                String updateSurname = surnameEditText.getText().toString();
                String updateFull = updateName + " " + updateSurname;

                if (updateName.isEmpty()) {
                    inputNameEditText.setError(getString(R.string.this_field_is_required));
                    inputNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"));
                    isError = true;
                }

                if (updateSurname.isEmpty()) {
                    inputSurnameEditText.setError(getString(R.string.this_field_is_required));
                    inputSurnameEditText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf"));
                    isError = true;
                }
                if (!isError) {

                    progress = ProgressDialog.show(this, "", getString(R.string.update_date_user_progress), true);

                    UpdateBuilder<User, ?> updateUser = dao.updateBuilder();
                    try {
                        updateUser.where().eq(User.Columns.FIREBASE_ID, userFirebaseId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    try {
                        updateUser.updateColumnValue(User.Columns.FULLNAME, updateFull);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    if (pickedImage != null) {

                        String pickedImageString = pickedImage.toString();

                        try {
                            updateUser.updateColumnValue(User.Columns.PHOTO_URL, pickedImageString);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(updateFull)
                                .setPhotoUri(pickedImage)
                                .build();

                        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ProfileActivity.this, R.string.text_user_success, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ProfileActivity.this, R.string.text_error_user, Toast.LENGTH_SHORT).show();
                                }
                                progress.dismiss();
                            }

                        });

                        try {
                            updateUser.update();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                    } else {

                        String photoUriString = photoUri.toString();

                        try {
                            updateUser.updateColumnValue(User.Columns.PHOTO_URL, photoUriString);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        final UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(updateFull)
                                .setPhotoUri(photoUri)
                                .build();

                        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ProfileActivity.this, R.string.text_user_success, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ProfileActivity.this, R.string.text_user_error_firebase, Toast.LENGTH_SHORT).show();
                                }
                                progress.dismiss();
                            }
                        });


                    }
                    try {
                        updateUser.update();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }

                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            pickedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            cursor.close();
            Glide.with(ProfileActivity.this).load(pickedImage).apply(RequestOptions.circleCropTransform()).into(myPictureImageView);

        }
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public void setDataUserDatabase() {
        userMy = new User();

        dbHelper = new DbHelper(this);

        try {
            dao = dbHelper.getDao(User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        userMy = dbHelper.getByUser(userFirebaseId);

        fullNameString = userMy.getFullName();
        photoString = userMy.getPhotoString();
        photoUri = Uri.parse(photoString);

        if (fullNameString != null) {
            String[] arrayName = fullNameString.split("\\s");
            nameString = arrayName[0];
            surnameString = arrayName[1];
            nameEditText.setText(nameString);
            surnameEditText.setText(surnameString);
        }

        if (photoUri != null) {
            Glide.with(ProfileActivity.this).load(photoUri).apply(RequestOptions.circleCropTransform()).into(myPictureImageView);
        } else {
            Glide.with(ProfileActivity.this).load(R.drawable.ic_user).into(myPictureImageView);
        }
    }

    public void setSettingPreferences() {
        MySharePreference mySharePreference = new MySharePreference();
        String languageString = mySharePreference.getLanguage(ProfileActivity.this);
        userFirebaseId = mySharePreference.getFirebaseId(ProfileActivity.this);
        setLocale(languageString);
    }

    public void authFirebaseSet() {
        auth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (user == null) {
                    startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}