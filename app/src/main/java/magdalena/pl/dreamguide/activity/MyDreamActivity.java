package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.inteface.MyDreamClickListener;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.MyDreamAdapter;
import magdalena.pl.dreamguide.app.Application;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.MyDream;
import magdalena.pl.dreamguide.model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyDreamActivity extends AppCompatActivity implements MyDreamClickListener {

    public static final int REQUEST_CODE_CREATE = 1;
    public static final int REQUEST_CODE_EDIT = 2;
    @BindView(R.id.myDreamRecyclerView)
    RecyclerView myDreamRecyclerView;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;
    private MyDreamAdapter myDreamAdapter;
    private List<MyDream> myDreamList;
    String idUserString;
    private Dao<MyDream, Integer> myDreamDao;
    private Dao<User, Integer> myUserDao;

    private String socialLink;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_dream);

        ButterKnife.bind(this);

        setToolbar();

        MySharePreference sharePreference = new MySharePreference();
        socialLink = sharePreference.getSocialLink(MyDreamActivity.this);
        idUserString = sharePreference.getFirebaseId(MyDreamActivity.this);
        String languageString = sharePreference.getLanguage(MyDreamActivity.this);
        setLocale(languageString);

        setDatabase();

        setRecycleView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_dream:
                addMyDream();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void addMyDream() {
        Intent intent = new Intent(this, AddEditDreamActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CREATE);
    }

    @Override
    public void onClick(View view, int position) {
        MyDream myDream = myDreamList.get(position);

        Intent intent = new Intent(this, AddEditDreamActivity.class);
        intent.putExtra("my_dream_id", myDream.getMyDreamId());
        intent.putExtra("title_dream", myDream.getTitleDream());
        intent.putExtra("description_dream", myDream.getDescriptionDream());
        intent.putExtra("symbol_dream", myDream.getSelectDream());
        intent.putExtra("positionEmotion", myDream.getPosition());
        intent.putExtra("socialLink", socialLink);

        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    @Override
    public void showMenu(View view, final int position) {

        final MyDream myDream = myDreamList.get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.delete_dream).setMessage(R.string.message_delete).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DeleteBuilder<MyDream, ?> deleteBuilder = myDreamDao.deleteBuilder();

                try {
                    deleteBuilder.where().eq(MyDream.Columns.MY_DREAM_ID, myDream.getMyDreamId());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    deleteBuilder.delete();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                myDreamAdapter.removeMyDream(position);

                Toast.makeText(MyDreamActivity.this, R.string.you_dream_was_delete, Toast.LENGTH_SHORT).show();

                if (myDreamList.size() <= 0) {

                    emptyLayout.setVisibility(View.VISIBLE);
                    myDreamRecyclerView.setVisibility(View.GONE);
                }
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setDatabase() {

        DbHelper dbHelper = new DbHelper(this);

        try {
            myDreamDao = dbHelper.getDao(MyDream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            myUserDao = dbHelper.getDao(User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        QueryBuilder<User, Integer> userQa = myUserDao.queryBuilder();

        try {
            userQa.where().eq(User.Columns.FIREBASE_ID, idUserString);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        QueryBuilder<MyDream, Integer> myDreamQa = myDreamDao.queryBuilder();

        try {
            myDreamList = myDreamQa.join(userQa).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void setRecycleView() {

        myDreamAdapter = new MyDreamAdapter(myDreamList);
        myDreamRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myDreamRecyclerView.setAdapter(myDreamAdapter);
        myDreamAdapter.setMyDreamClickListener(this);

        if (myDreamList != null && myDreamList.size() > 0) {
            emptyLayout.setVisibility(View.GONE);
            myDreamRecyclerView.setVisibility(View.VISIBLE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            myDreamRecyclerView.setVisibility(View.GONE);
        }
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == AddEditDreamActivity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_CREATE) {
                if (data != null) {
                    MyDream myDream = data.getParcelableExtra("dream");
                    myDreamAdapter.addMyDream(myDream);
                    myDreamAdapter.notifyDataSetChanged();
                    emptyLayout.setVisibility(View.GONE);
                    myDreamRecyclerView.setVisibility(View.VISIBLE);
                    Toast.makeText(this, R.string.dream_was_added, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


}
