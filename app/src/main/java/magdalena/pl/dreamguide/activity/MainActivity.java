package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.app.Application;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.fragment.AboutApplicationFragment;
import magdalena.pl.dreamguide.fragment.ContactFragment;
import magdalena.pl.dreamguide.fragment.DreamFragment;
import magdalena.pl.dreamguide.fragment.StatisticFragment;
import magdalena.pl.dreamguide.model.User;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MySharePreference mySharePreference;
    private String photoStringDefault = "https://firebasestorage.googleapis.com/v0/b/dream-guide-5e482.appspot.com/o/user.png?alt=media&token=8ff76dc3-c389-412f-a0ff-58e24fec8a64";
    NavigationView navigationView;
    View headerView;
    private Dao<User, ?> dao;
    List<User> userList = null;
    String idUserString, photoUriString;
    FirebaseAuth auth;
    FirebaseUser user;
    private ImageView imageViewUser;
    private TextView fullNameTextView;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mySharePreference = new MySharePreference();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String socialLink = getIntent().getStringExtra("social-link");
            mySharePreference.saveSocialLink(MainActivity.this, socialLink);
        }

        String languageString = mySharePreference.getLanguage(MainActivity.this);
        setLocale(languageString);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_dreams);

        headerView = navigationView.getHeaderView(0);
        imageViewUser = (ImageView) headerView.findViewById(R.id.personImageView);
        fullNameTextView = (TextView) headerView.findViewById(R.id.fullNameTextView);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DreamFragment(), "List_dream")
                    .commit();
        }

        setUserDataSetting();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            this.finishAffinity();
            super.onBackPressed();

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_dreams:
                showFragment(new DreamFragment());
                break;
            case R.id.nav_my_dreams:
                startActivity(new Intent(this, MyDreamActivity.class));
                break;
            case R.id.nav_contact:
                showFragment(new ContactFragment());
                break;
            case R.id.nav_statistic:
                showFragment(new StatisticFragment());
                break;
            case R.id.nav_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.nav_settings:

                startActivity(new Intent(this, SettingActivity.class));
                break;
            case R.id.nav_aboutApplication:
                showFragment(new AboutApplicationFragment());
                break;
            case R.id.nav_logout:

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle(R.string.logout).setMessage(R.string.message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();

                        goLoginScreen();
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialogBuilder.create();
                alertDialogBuilder.show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            try {
                this.finishAffinity();
            } catch (Exception e) {
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setUserDataSetting() {

        DbHelper dbHelper = new DbHelper(this);
        User myUser = new User();

        try {
            dao = dbHelper.getDao(User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {

            idUserString = user.getUid();
            mySharePreference.saveFirebaseId(MainActivity.this, idUserString);

            try {
                userList = dao.queryForEq(User.Columns.FIREBASE_ID, idUserString);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (userList != null && !userList.isEmpty()) {

                myUser = dbHelper.getByUser(idUserString);
                String fullNameDataBase = myUser.getFullName();
                String emailDataBase = myUser.getEmail();
                String photoUrl = myUser.getPhotoString();
                mySharePreference.saveFirebaseId(MainActivity.this, idUserString);

                if (fullNameDataBase != null) {

                    fullNameTextView.setText(fullNameDataBase);
                    String[] arrayName = fullNameDataBase.split("\\s");
                    String nameString = arrayName[0];
                    mySharePreference.saveName(MainActivity.this, nameString);
                } else {
                    String emptyUserString = getText(R.string.userTextView).toString();
                    mySharePreference.saveName(MainActivity.this, emptyUserString);
                    fullNameTextView.setText(emailDataBase);
                }

                if (photoUrl != null) {
                    Glide.with(MainActivity.this).load(Uri.parse(photoUrl)).apply(RequestOptions.circleCropTransform()).into(imageViewUser);
                } else {
                    Glide.with(MainActivity.this).load(R.drawable.ic_user).into(imageViewUser);
                }

            } else {

                String fullNameString = user.getDisplayName();
                String emailString = user.getEmail();
                Uri photoUrl = user.getPhotoUrl();

                if (photoUrl != null) {

                    String photoUrlString = photoUrl.toString();

                    try {
                        dao.create(User.fromUser(idUserString, fullNameString, emailString, photoUrlString));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        dao.create(User.fromUser(idUserString, fullNameString, emailString, photoStringDefault));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (fullNameString != null) {
                    fullNameTextView.setText(fullNameString);
                } else {
                    fullNameTextView.setText(emailString);
                }

                if (photoUrl != null) {
                    Glide.with(MainActivity.this).load(photoUrl).apply(RequestOptions.circleCropTransform()).into(imageViewUser);
                } else {
                    Glide.with(MainActivity.this).load(R.drawable.ic_user).into(imageViewUser);
                }
                mySharePreference.saveFirebaseId(MainActivity.this, idUserString);
            }
        } else {
            goLoginScreen();
        }
    }

    private void goLoginScreen() {

        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        mySharePreference.clearSocialLink(MainActivity.this);
                        mySharePreference.clearFirebaseId(MainActivity.this);

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        MainActivity.this.finish();
                    }
                });
    }


    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}