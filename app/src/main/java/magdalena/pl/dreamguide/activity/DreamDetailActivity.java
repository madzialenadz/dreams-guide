package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DreamDetailActivity extends AppCompatActivity  {

    String dreamsTitle, dreamImageUrl, dreamsDescription;
    @BindView(R.id.dreamImageView)
    ImageView dreamImageView;
    @BindView(R.id.dreamsTitleTextView)
    TextView dreamsTitleTextView;
    @BindView(R.id.dreamsDescribeTextView)
    TextView dreamsDescribeTextView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dream_detail);

        setToolbar();

        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dreamImageUrl = getIntent().getStringExtra("dreamImageUrl");
            dreamsTitle = getIntent().getStringExtra("dreamName");
            dreamsDescription = getIntent().getStringExtra("dreamDetails");

            setImageDream(dreamImageUrl);

            dreamsDescribeTextView.setText(dreamsDescription);
            dreamsTitleTextView.setText(dreamsTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);

    }


    public void setImageDream(String imageUrl) {

        Glide.with(DreamDetailActivity.this).load(imageUrl).into(dreamImageView);
        dreamsDescribeTextView.setText(dreamsDescription);
        dreamsTitleTextView.setText(dreamsTitle);
    }
}