package magdalena.pl.dreamguide.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;
import com.dpizarro.autolabel.library.Label;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.EmotionAdapter;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.Emotion;
import magdalena.pl.dreamguide.model.MyDream;
import magdalena.pl.dreamguide.model.User;
import magdalena.pl.dreamguide.utils.ConstansKt;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddEditDreamActivity extends AppCompatActivity {

    @BindView(R.id.errorEmotionText)
    TextView errorEmotionText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tagDreamSymbol)
    AutoLabelUI tagDreamSymbol;
    @BindView(R.id.addTagDream)
    TextView addTagDream;
    @BindView(R.id.errorTagDream)
    TextView errorTagDream;
    @BindView(R.id.contentEditDreamShare)
    CoordinatorLayout contentEditDreamShare;

    private String firebaseId;
    private CallbackManager callbackManager;
    private static final int SELECTED_DREAM = 0;
    private String socialLink;
    @BindView(R.id.titleActivity)
    TextView titleActivity;

    private List<Emotion> emotionStatus = new ArrayList<>();
    private EmotionAdapter emotionAdapter;

    User currentUser;
    DbHelper dbHelper;
    QueryBuilder<User, Integer> userQa;
    QueryBuilder<MyDream, Integer> myDreamQa;
    private Dao<MyDream, Integer> myDreamDao;
    private Dao<User, Integer> myUserDao;
    List<MyDream> myDreamList = null;
    @BindView(R.id.dreamTitleEditText)
    TextInputEditText dreamTitleEditText;
    @BindView(R.id.inputDreamTitleEditText)
    TextInputLayout inputDreamTitleEditText;
    @BindView(R.id.descriptionDreamEditText)
    TextInputEditText descriptionDreamEditText;
    @BindView(R.id.inputDescriptionDreamEditText)
    TextInputLayout inputDescriptionDreamEditText;
    @BindView(R.id.saveDreamButton)
    TextView saveDreamButton;
    @BindView(R.id.emotionList)
    RecyclerView emotionRecyclerView;
    String dreamTitleEdit, dreamDescriptionEdit, symbolDreamEdit, selectSymbolDream, dreamTitle, dreamDescription, positionStringDream, positionDreamUpdate;
    int dreamId, positionEmotion, positionEmotionUpdate;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dream);

        setToolbar();

        ButterKnife.bind(this);

        socialLink = "addDream";

        setPersonalSetting();

        setDatabase();

        setEmotionRecycleView();

        setCustomTagSymbolDream();

        inputDreamTitleEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
            }
        });
        inputDescriptionDreamEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);
            }
        });

        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            dreamId = extras.getInt("my_dream_id");
            dreamTitleEdit = extras.getString("title_dream");
            dreamDescriptionEdit = extras.getString("description_dream");
            symbolDreamEdit = extras.getString("symbol_dream");
            positionDreamUpdate = extras.getString("positionEmotion");
            socialLink = extras.getString("socialLink");

            emotionAdapter.setSelectedEmotion(positionEmotionUpdate);
            String[] symbolDreamArray = symbolDreamEdit.split(",");

            for (String aSymbolDreamArray : symbolDreamArray) {
                tagDreamSymbol.addLabel(aSymbolDreamArray);
            }

            dreamTitleEditText.setText(dreamTitleEdit);
            descriptionDreamEditText.setText(dreamDescriptionEdit);
            titleActivity.setText(R.string.editDream);

            dreamTitleEditText.setEnabled(false);
            dreamTitleEditText.setFocusableInTouchMode(true);
            descriptionDreamEditText.setEnabled(false);
            descriptionDreamEditText.setFocusableInTouchMode(true);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_dream, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem itemShare = menu.findItem(R.id.share_dream);
        MenuItem itemEdit = menu.findItem(R.id.editDream);
        switch (socialLink) {
            case "email":
                itemShare.setVisible(false);
                itemEdit.setVisible(true);
                return true;
            case "addDream":
                itemShare.setVisible(false);
                itemEdit.setVisible(false);
                return true;
            default:
                itemShare.setVisible(true);
                itemEdit.setVisible(true);
                return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share_dream:
                switch (socialLink) {
                    case "google_plus":

                        return true;
                    case "facebook":

                        FacebookSdk.sdkInitialize(getApplicationContext());
                        callbackManager = CallbackManager.Factory.create();
                        ShareDialog shareDialog = new ShareDialog(this);
                        shareDialog.registerCallback(callbackManager, callback);
                        shareDreamDialogFacebook();

                        return true;
                }
                break;
            case R.id.editDream:

                dreamTitleEditText.setEnabled(true);
                dreamTitleEditText.setFocusable(true);
                descriptionDreamEditText.setEnabled(true);
                descriptionDreamEditText.setFocusable(true);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.addTagDream, R.id.saveDreamButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addTagDream:
                Intent intent = new Intent(AddEditDreamActivity.this, SelectDreamActivity.class);
                startActivityForResult(intent, SELECTED_DREAM);
                break;
            case R.id.saveDreamButton:
                boolean hasError = false;

                dreamTitle = dreamTitleEditText.getText().toString();
                dreamDescription = descriptionDreamEditText.getText().toString();

                List<Label> labels = tagDreamSymbol.getLabels();

                if (dreamTitle.isEmpty()) {
                    inputDreamTitleEditText.setError(getString(R.string.this_field_is_required));
                    inputDreamTitleEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (dreamDescription.isEmpty()) {
                    inputDescriptionDreamEditText.setError(getString(R.string.this_field_is_required));
                    inputDescriptionDreamEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (emotionAdapter.selectedPosition() != -1) {

                    Emotion emotion = emotionAdapter.getSelectedEmotionItem();

                    positionEmotion = emotionAdapter.selectedPosition();
                    positionStringDream = String.valueOf(positionEmotion);

                } else {
                    errorEmotionText.setVisibility(View.VISIBLE);
                    errorEmotionText.setText(R.string.please_select_emotion);
                    hasError = true;
                }

                if (labels.size() == 0) {

                    errorTagDream.setVisibility(View.VISIBLE);
                    errorTagDream.setText(R.string.you_did_select_symbol_dream);
                    hasError = true;

                } else {

                    StringBuilder sB = new StringBuilder();

                    for (Label label : labels) {
                        sB.append(label.getText()).append(",");
                    }

                    selectSymbolDream = sB.toString();
                }

                if (!hasError) {

                    try {
                        myDreamList = myDreamDao.queryForEq(MyDream.Columns.MY_DREAM_ID, dreamId);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    if (myDreamList != null && !myDreamList.isEmpty()) {

                        UpdateBuilder<MyDream, ?> updateMyDream = myDreamDao.updateBuilder();

                        try {
                            updateMyDream.where().eq(MyDream.Columns.MY_DREAM_ID, dreamId);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        try {
                            updateMyDream.updateColumnValue(MyDream.Columns.TITLE_DREAM, dreamTitle);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        try {
                            updateMyDream.updateColumnValue(MyDream.Columns.DESCRIPTION, dreamDescription);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        try {
                            updateMyDream.updateColumnValue(MyDream.Columns.SELECT_POSITION_EMOTION, positionStringDream);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        try {
                            updateMyDream.updateColumnValue(MyDream.Columns.SYMBOL_DREAM, selectSymbolDream);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        try {
                            updateMyDream.update();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        Intent back = new Intent(AddEditDreamActivity.this, MyDreamActivity.class);
                        back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(back);
                        AddEditDreamActivity.this.finish();


                    } else {

                        MyDream myDream = new MyDream(currentUser, dreamTitle, dreamDescription, selectSymbolDream, positionStringDream, getCurrentDate());

                        try {
                            myDreamDao.create(myDream);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        Intent saveDream = new Intent();
                        saveDream.putExtra("dream", myDream);
                        setResult(RESULT_OK, saveDream);
                        finish();
                    }
                }
                break;
        }
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private String getCurrentDate() {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("EEEE dd.MM.yyyy");
        String dayOfWeek = ft.format(date);
        String changeDayOfWeek = dayOfWeek.replace(dayOfWeek.substring(0, 1), dayOfWeek.substring(0, 1).toUpperCase());
        return changeDayOfWeek;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECTED_DREAM) {
                if (data != null) {
                    String selectedDream = data.getStringExtra("SYMBOL_DREAM");
                    List<Label> symbolDreamTag = tagDreamSymbol.getLabels();
                    if (!containsTagDream(symbolDreamTag, selectedDream)) {
                        tagDreamSymbol.addLabel(selectedDream);
                    }
                }
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    boolean containsTagDream(List<Label> list, String symbolDream) {
        for (Label label : list) {
            if (label.getText().equals(symbolDream)) {
                return true;
            }
        }
        return false;
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setEmotionRecycleView() {

        Emotion emotion = new Emotion(R.drawable.ic_happy, R.string.happy);
        emotionStatus.add(emotion);
        emotion = new Emotion(R.drawable.ic_none, R.string.none);
        emotionStatus.add(emotion);
        emotion = new Emotion(R.drawable.ic_scare, R.string.scared);
        emotionStatus.add(emotion);
        emotion = new Emotion(R.drawable.ic_sad, R.string.sad);
        emotionStatus.add(emotion);
        emotion = new Emotion(R.drawable.ic_angry, R.string.angry);
        emotionStatus.add(emotion);

        emotionAdapter = new EmotionAdapter(emotionStatus);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(this, 5);
        emotionRecyclerView.setLayoutManager(linearLayoutManager);
        emotionRecyclerView.setAdapter(emotionAdapter);
    }

    public void setPersonalSetting(){

        MySharePreference sharePreference = new MySharePreference();

        firebaseId = sharePreference.getFirebaseId(AddEditDreamActivity.this);
        String languageString = sharePreference.getLanguage(AddEditDreamActivity.this);
        setLocale(languageString);
    }



    public void setDatabase() {

        dbHelper = new DbHelper(this);
        currentUser = new User();
        currentUser = dbHelper.getByUser(firebaseId);


        try {
            myDreamDao = dbHelper.getDao(MyDream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            myUserDao = dbHelper.getDao(User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        userQa = myUserDao.queryBuilder();

        try {
            userQa.where().eq(User.Columns.FIREBASE_ID, firebaseId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        myDreamQa = myDreamDao.queryBuilder();

        try {
            myDreamList = myDreamQa.join(userQa).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void setCustomTagSymbolDream() {

        AutoLabelUISettings autoLabelUISettings = new AutoLabelUISettings.Builder()
                .withMaxLabels(3)
                .withIconCross(R.drawable.cross)
                .withBackgroundResource(R.drawable.tagdream)
                .withLabelsClickables(false)
                .withLabelPadding(20)
                .withTextSize(R.dimen.tag_dream)
                .build();
        tagDreamSymbol.setSettings(autoLabelUISettings);
    }

    private void shareDreamDialogFacebook() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.sharedialogfacebook, null);
        alertDialog.setView(dialogView);
        alertDialog.show();
        alertDialog.create();

        View rootView = findViewById(R.id.contentEditDreamShare).getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap image = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.destroyDrawingCache();

        SharePhoto dream = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("")
                .build();

        SharePhotoContent contentDream = new SharePhotoContent.Builder()
                .addPhoto(dream)
                .build();

        final ShareButton shareButton = (ShareButton) dialogView.findViewById(R.id.fb_share_button);
        shareButton.setShareContent(contentDream);

        ImageView customShareButton = (ImageView) dialogView.findViewById(R.id.fb_share);
        customShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareButton.performClick();
            }
        });
    }

    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Toast.makeText(AddEditDreamActivity.this, R.string.text_symbol_dream_success, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {
            Toast.makeText(AddEditDreamActivity.this, R.string.share_symbol_dream_canceled, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(AddEditDreamActivity.this, R.string.share_dream_error, Toast.LENGTH_SHORT).show();
        }
    };

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}