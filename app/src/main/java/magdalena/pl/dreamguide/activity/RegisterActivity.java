package magdalena.pl.dreamguide.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.dialog.ErrorDialogNetwork;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.network.Network;

import magdalena.pl.dreamguide.utils.ConstansKt;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "Log: ";
    ProgressDialog progress;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    private String email;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.inputEmailEditText)
    TextInputLayout inputEmailEditText;
    @BindView(R.id.PaswordEditText)
    EditText passwordEditText;
    @BindView(R.id.inputPasswordEditText)
    TextInputLayout inputPasswordEditText;
    @BindView(R.id.inputConfirmPasswordEditText)
    TextInputLayout inputConfirmPasswordEditText;
    @BindView(R.id.createAccountTextView)
    TextView createAccountTextView;
    @BindView(R.id.checkableCustomFontTextView5)
    TextView checkableCustomFontTextView5;
    @BindView(R.id.TermsofUseLayout)
    LinearLayout TermsOfUseLayout;
    @BindView(R.id.confirmPasswordEditText)
    EditText confirmPasswordEditText;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setToolbar();

        setLanguge();

        authFirebaseSet();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String emailString = extras.getString("emailStringRegister");
            emailEditText.setText(emailString);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.createAccountTextView, R.id.TermsofUseLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.createAccountTextView:
                email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();

                boolean hasError = false;

                if (Network.isNetworkConnect(this)) {
                    FragmentManager fm = getFragmentManager();
                    ErrorDialogNetwork errorDialogNetwork = new ErrorDialogNetwork();
                    errorDialogNetwork.show(fm, "abc");
                    hasError = true;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    inputEmailEditText.setError(getString(R.string.this_email_is_incorrect));
                    inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(),ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (email.isEmpty()) {
                    inputEmailEditText.setError(getString(R.string.this_field_is_required));
                    inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (password.isEmpty()) {
                    inputPasswordEditText.setError(getString(R.string.this_field_is_required));
                    inputPasswordEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }
                if (confirmPassword.isEmpty()) {
                    inputConfirmPasswordEditText.setError(getString(R.string.this_field_is_required));
                    inputConfirmPasswordEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (!password.equals(confirmPassword)) {
                    showAlterDialog(R.string.error_password, R.string.password_are_not_the_same, RegisterActivity.class);
                    hasError = true;
                }

                if (!hasError) {
                    progress = ProgressDialog.show(this, "", getString(R.string.register_user_dialog), true);
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        if (task.getException() instanceof FirebaseAuthException) {
                                            showAlterDialog(R.string.titleErrorRegisterDialog, R.string.messageErrorRegisterDialog, RegisterActivity.class);
                                        }
                                    } else {
                                        showAlterDialog(R.string.titleSuccesssfulDialog, R.string.messageSuccesssfulDialog, SignInActivity.class);
                                    }
                                    progress.dismiss();
                                }
                            });
                }
                break;
            case R.id.TermsofUseLayout:
                break;
        }
    }

    public void showAlterDialog(int title, int message, final Class myClass) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (myClass.equals(RegisterActivity.class)) {
                    dialogInterface.dismiss();
                } else if (myClass.equals(SignInActivity.class)) {
                    Intent intent = new Intent(RegisterActivity.this, SignInActivity.class);
                    intent.putExtra("emailStringRegister", email);
                    startActivity(intent);
                }
            }
        });

        builder.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @OnClick(R.id.privacy_policy)
    public void onViewClicked() {
        Uri url = Uri.parse("http://dreamsguide.pl/privacy-policy/");
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }


    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void authFirebaseSet() {

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    public void setLanguge() {
        MySharePreference mySharePreference = new MySharePreference();
        String languageString = mySharePreference.getLanguage(RegisterActivity.this);
        setLocale(languageString);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}