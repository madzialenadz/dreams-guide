package magdalena.pl.dreamguide.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.dialog.ErrorDialogNetwork;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.network.Network;
import magdalena.pl.dreamguide.utils.ConstansKt;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResetPasswordActivity extends AppCompatActivity {

    ProgressDialog pD;
    private FirebaseAuth auth;
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.inputEmailEditText)
    TextInputLayout inputEmailEditText;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        setToolbar();

        setLanguage();

        auth = FirebaseAuth.getInstance();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String email = extras.getString("emailString");
            emailEditText.setText(email);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.recoverPasswordButton)
    public void onViewClicked() {
        boolean hasError = false;
        String emailTextView = emailEditText.getText().toString();
        if (Network.isNetworkConnect(this)) {
            FragmentManager fm = getFragmentManager();
            ErrorDialogNetwork errorDialogNetwork = new ErrorDialogNetwork();
            errorDialogNetwork.show(fm, "abc");
            hasError = true;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailTextView).matches()) {
            inputEmailEditText.setError(getString(R.string.this_email_is_incorrect));
            inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
            hasError = true;
        }
        if (emailTextView.isEmpty()) {
            inputEmailEditText.setError(getString(R.string.this_field_is_required));
            inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
            hasError = true;
        }
        if (!hasError) {
            pD = ProgressDialog.show(this, "", getString(R.string.send_link));
            auth.sendPasswordResetEmail(emailTextView).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        showAlterDialog("Link has been sent.", "Link to change your password has been sent.");
                    } else {
                        showAlterDialog("Opss!!", "Failed to send reset email!. Probably the email that you want to reset your password does not exist.");
                    }
                    pD.dismiss();
                }
            });
        }
    }



    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
    }

    public void setLanguage(){
        MySharePreference mySharePreference = new MySharePreference();
        String languageString = mySharePreference.getLanguage(ResetPasswordActivity.this);
        setLocale(languageString);
    }


    public void showAlterDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(title).setMessage(message).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                pD.dismiss();
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}