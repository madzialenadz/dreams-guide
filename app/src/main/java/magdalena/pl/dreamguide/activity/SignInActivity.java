package magdalena.pl.dreamguide.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import magdalena.pl.dreamguide.utils.ConstansKt;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.dialog.ErrorDialogNetwork;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.dialog.ProblemSignInFragment;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.network.Network;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivity extends AppCompatActivity {

    MySharePreference mySharePreference;
    private static final String TAG = "Log: ";
    @BindView(R.id.singUpButton)
    TextView singUpButton;
    @BindView(R.id.privacy_policy_Text)
    TextView privacyPolicyText;
    private String emailString, passwordString;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.emailEditText)
    TextInputEditText emailEditText;
    @BindView(R.id.passwordEditText)
    TextInputEditText passwordEditText;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    ProgressDialog pD;
    @BindView(R.id.inputEmailEditText)
    TextInputLayout inputEmailEditText;
    @BindView(R.id.inputPasswordEditText)
    TextInputLayout inputPasswordEditText;
    @BindView(R.id.signInButton)
    TextView signInButton;
    @BindView(R.id.forgotPasswordButton)
    TextView forgotPasswordButton;
    @BindView(R.id.TermsofUseLayout)
    LinearLayout TermsOfUseLayout;
    String emailStringSharePreference;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //private String generalFontMontserrat = "fonts/montserrat_regular.ttf";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign);

        setToolbar();

        ButterKnife.bind(this);

        authFirebaseSet();

        mySharePreference = new MySharePreference();
        String languageString = mySharePreference.getLanguage(SignInActivity.this);
        setLocale(languageString);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String emailStringExtras = extras.getString("emailStringRegister");
            emailEditText.setText(emailStringExtras);
        }

        emailStringSharePreference = mySharePreference.getEmailUser(SignInActivity.this);

        if (!emailStringSharePreference.isEmpty()) {
            emailEditText.setText(emailStringSharePreference);
        }

    }

    @OnClick({R.id.signInButton, R.id.forgotPasswordButton, R.id.singUpButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signInButton:
                boolean hasError = false;
                emailString = emailEditText.getText().toString();
                passwordString = passwordEditText.getText().toString();

                if (Network.isNetworkConnect(this)) {
                    FragmentManager fm = getFragmentManager();
                    ErrorDialogNetwork errorDialogNetwork = new ErrorDialogNetwork();
                    errorDialogNetwork.show(fm, "abc");
                    hasError = true;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
                    inputEmailEditText.setError(getString(R.string.this_email_is_incorrect));
                    inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (emailString.isEmpty()) {
                    inputEmailEditText.setError(getString(R.string.this_field_is_required));
                    inputEmailEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (passwordString.isEmpty()) {
                    inputPasswordEditText.setError(getString(R.string.this_field_is_required));
                    inputPasswordEditText.setTypeface(Typeface.createFromAsset(getAssets(), ConstansKt.generalFontMontserratRegular));
                    hasError = true;
                }

                if (!hasError) {

                    pD = ProgressDialog.show(this, "", getString(R.string.loading));
                    mAuth.signInWithEmailAndPassword(emailString, passwordString)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (!task.isSuccessful()) {
                                        Log.w(TAG, "signInWithEmail:failed", task.getException());
                                        FragmentManager fm = getFragmentManager();
                                        ProblemSignInFragment problemSignInFragment = new ProblemSignInFragment();
                                        problemSignInFragment.show(fm, "");
                                        mySharePreference.clearEmailUser(SignInActivity.this);

                                    } else {
                                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                                        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("social-link", "email");
                                        startActivity(intent);
                                        mySharePreference.saveEmailUser(SignInActivity.this, emailString);
                                        mySharePreference.saveSocialLink(SignInActivity.this, "email");

                                    }
                                    pD.dismiss();
                                }
                            });
                }
                break;
            case R.id.forgotPasswordButton:
                String emailText = emailEditText.getText().toString();
                Intent start = new Intent(SignInActivity.this, ResetPasswordActivity.class);
                start.putExtra("emailString", emailText);
                startActivity(start);
                break;

            case R.id.singUpButton:
                Intent intent = new Intent(SignInActivity.this, RegisterActivity.class);
                intent.putExtra("emailStringRegister", emailString);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @OnClick(R.id.privacy_policy_Text)
    public void onViewClicked() {
        Uri url = Uri.parse("http://dreamsguide.pl/privacy-policy/");
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }

    public void authFirebaseSet() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Intent emailIntent = new Intent(SignInActivity.this, MainActivity.class);
                    emailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    emailIntent.putExtra("social-link", "email");
                    startActivity(emailIntent);
                }

            }
        };
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        setSupportActionBar(toolbar);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}