package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.SectionsPagerAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SwipeActivity extends AppCompatActivity {

    @BindView(R.id.container)
    ViewPager viewPager;
    @BindView(R.id.ImageViewCircle1)
    ImageView ImageViewCircle1;
    @BindView(R.id.ImageViewCircle2)
    ImageView ImageViewCircle2;
    @BindView(R.id.ImageViewCircle3)
    ImageView ImageViewCircle3;
    @BindView(R.id.searchDreamButton)
    Button searchDreamButton;
    @BindView(R.id.singInButton)
    Button singInButton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private MySharePreference mySharePreference;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        ButterKnife.bind(this);
        mySharePreference = new MySharePreference();
        authFirebaseSet();
        setLanguage();
        setViewPager();
        setImageResource();
        setTab();



    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    @OnClick({R.id.ImageViewCircle1, R.id.ImageViewCircle2, R.id.ImageViewCircle3, R.id.searchDreamButton, R.id.singInButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ImageViewCircle1:
                ImageViewCircle1.setImageResource(R.drawable.ic_circle_gray);
                viewPager.setCurrentItem(0);
                break;
            case R.id.ImageViewCircle2:
                ImageViewCircle2.setImageResource(R.drawable.ic_circle_gray);
                viewPager.setCurrentItem(1);
                break;
            case R.id.ImageViewCircle3:
                ImageViewCircle3.setImageResource(R.drawable.ic_circle_gray);
                viewPager.setCurrentItem(2);
                break;
            case R.id.searchDreamButton:
                Intent intent = new Intent(this, InterpretDreamActivity.class);
                startActivity(intent);
                break;
            case R.id.singInButton:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    public void authFirebaseSet() {

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    String socialLink = mySharePreference.getSocialLink(SwipeActivity.this);

                    if (socialLink != null) {

                        switch (socialLink) {
                            case "email":

                                Intent emailIntent = new Intent(SwipeActivity.this, MainActivity.class);
                                emailIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                emailIntent.putExtra("social-link", "email");
                                startActivity(emailIntent);

                                break;
                            case "facebook":

                                Intent facebookIntent = new Intent(SwipeActivity.this, MainActivity.class);
                                facebookIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                facebookIntent.putExtra("social-link", "facebook");
                                startActivity(facebookIntent);

                                break;

                            case "google-plus":


                                break;
                        }
                    }
                }
            }
        };

    }

    private void btnAction(int action) {
        switch (action) {
            case 0:
                ImageViewCircle1.setImageResource(R.drawable.ic_circle_gray);
                break;
            case 1:
                ImageViewCircle2.setImageResource(R.drawable.ic_circle_gray);
                break;
            case 2:
                ImageViewCircle3.setImageResource(R.drawable.ic_circle_gray);
                break;
        }

    }

    public void setImageResource() {
        ImageViewCircle1.setImageResource(R.drawable.ic_circle_gray);
        ImageViewCircle2.setImageResource(R.drawable.ic_circle_black);
        ImageViewCircle3.setImageResource(R.drawable.ic_circle_black);
    }

    public void setLanguage() {
        MySharePreference mySharePreference = new MySharePreference();
        String languageString = mySharePreference.getLanguage(SwipeActivity.this);
        setLocale(languageString);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private void setTab() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {
                ImageViewCircle1.setImageResource(R.drawable.ic_circle_black);
                ImageViewCircle2.setImageResource(R.drawable.ic_circle_black);
                ImageViewCircle3.setImageResource(R.drawable.ic_circle_black);
                btnAction(position);
            }
        });
    }


    public void setViewPager() {
        viewPager = (ViewPager) findViewById(R.id.container);
        SectionsPagerAdapter mAdapter = new SectionsPagerAdapter(SwipeActivity.this, getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
    }


}