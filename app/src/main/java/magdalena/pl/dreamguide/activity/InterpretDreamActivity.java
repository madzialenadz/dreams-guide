package magdalena.pl.dreamguide.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.DreamAdapter;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.Dream;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InterpretDreamActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.recycleItemDream)
    RecyclerView recycleItemDream;

    private List<Dream> dreamList;
    private DbHelper dbHelper;
    private Dao<Dream, Integer> dreamDao;
    private DreamAdapter interpretedDreamAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interpret_dream);
        ButterKnife.bind(this);
        setToolbar();
        setLanguage();
        setDatabase();
        setRecycleView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dream_not_login, menu);
        MenuItem searchItem = menu.findItem(R.id.searchDream);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.search_dream));
        searchView.setOnQueryTextListener(this);
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign:
                startActivity(new Intent(this, LoginActivity.class));
                return true;
            default:
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(newText);
        return false;
    }

    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setLanguage() {
        MySharePreference sharePreference = new MySharePreference();
        String languageString = sharePreference.getLanguage(InterpretDreamActivity.this);
        setLocale(languageString);
    }

    void filter(String text) {
        List<Dream> dream = new ArrayList<>();
        for (Dream d : dreamList) {
            if (d.getNameDream().toLowerCase().contains(text)) {
                dream.add(d);
            }
        }
        interpretedDreamAdapter.updateList(dream);
    }

    public void setRecycleView() {

        int orientation = this.getResources().getConfiguration().orientation;

        if (dreamList != null && dreamList.size() > 0) {
            interpretedDreamAdapter = new DreamAdapter(dreamList, this);
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            float yInches = metrics.heightPixels / metrics.ydpi;
            float xInches = metrics.widthPixels / metrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);

            if (diagonalInches >= 6.5) {
                linearLayoutManager = new GridLayoutManager(this, 3);
                recycleItemDream.setLayoutManager(linearLayoutManager);
            } else {
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    linearLayoutManager = new GridLayoutManager(this, 2);
                    recycleItemDream.setLayoutManager(linearLayoutManager);
                } else {
                    linearLayoutManager = new GridLayoutManager(this, 3);
                    recycleItemDream.setLayoutManager(linearLayoutManager);
                }
            }
            recycleItemDream.setAdapter(interpretedDreamAdapter);

        } else {
            Toast.makeText(this, R.string.list_dream, Toast.LENGTH_SHORT).show();
        }
    }

    public void setDatabase() {

        dbHelper = new DbHelper(this);

        try {
            dreamDao = dbHelper.getDao(Dream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            dreamList = dreamDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}