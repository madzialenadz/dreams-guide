package magdalena.pl.dreamguide.app;


import android.content.Context;

import com.facebook.FacebookSdk;

import api.model.WeatherApi;
import magdalena.pl.dreamguide.R;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Application extends android.app.Application {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String API_KEY = "28e9cd1f10e044adfc293273382c0718";

    Retrofit retrofit;
    public static WeatherApi weatherApi;


    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        weatherApi = retrofit.create(WeatherApi.class);

        FacebookSdk.sdkInitialize(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/montserrat_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

}