package magdalena.pl.dreamguide.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;


public class SelectedLanguageDialogFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final MySharePreference mySharePreference = new MySharePreference();
        final TextView selectedLanguageTextView = (TextView) getActivity().findViewById(R.id.selectedLanguageTextView);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.selectedLanguage);
        builder.setItems(R.array.language, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        mySharePreference.saveLanguage(getActivity(), "en");
                        selectedLanguageTextView.setText(R.string.englishLanguage);
                        Runtime.getRuntime().exit(0);

                        break;
                    case 1:
                        mySharePreference.saveLanguage(getActivity(), "pl");
                        selectedLanguageTextView.setText(R.string.polishLanguage);
                        Runtime.getRuntime().exit(0);
                        break;
                }
            }
        });
        return builder.create();
    }
}
