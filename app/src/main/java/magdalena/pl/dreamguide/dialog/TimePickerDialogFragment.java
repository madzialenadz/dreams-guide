package magdalena.pl.dreamguide.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.activity.SettingActivity;

/**
 * Created by magdalenadziesinska on 14/03/2018.
 */

public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    MySharePreference mySharePreference;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT
                , this, hour, minute, DateFormat.is24HourFormat(getActivity()));

        return tpd;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        mySharePreference = new MySharePreference();

        TextView setTimeIncentive = (TextView) getActivity().findViewById(R.id.setTimeIncentiveTextView);

        String aMpM = "AM";
        if (hourOfDay > 11) {
            aMpM = "PM";
        }

        int currentHour;
        if (hourOfDay > 11) {
            currentHour = hourOfDay - 12;
        } else {
            currentHour = hourOfDay;
        }

        String saveTime = String.valueOf(currentHour) + ":" + String.valueOf(minute) + " " + aMpM;
        mySharePreference.saveTimeIncetive(getActivity(), saveTime);
        setTimeIncentive.setText(saveTime);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Toast.makeText(getActivity(), R.string.set_time_cancel, Toast.LENGTH_LONG).show();
        super.onCancel(dialog);
    }
}
