package magdalena.pl.dreamguide.model;

import com.j256.ormlite.field.DatabaseField;

public class Category {

    public class Columns {
        public static final String CATEGORY_ID = "category_id";
        public static  final  String CATEGORY_NAME = "category_name";
    }

    @DatabaseField(generatedId = true,columnName = Columns.CATEGORY_ID)
    private int categoryId;
    @DatabaseField(columnName = Columns.CATEGORY_NAME)
    private String categoryName;

    public Category() {
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
