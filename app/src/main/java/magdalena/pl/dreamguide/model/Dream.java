package magdalena.pl.dreamguide.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "dream")
public class Dream {

    public class Columns {
        public static final String DREAM_ID = "dream_id";
        public static final String DESCRIPTION = "description_dreams";
        public static final String IMAGE_URL = "image_url";
        public static final String NAME_DREAM = "name_dream";
        public static final String IS_SELECTED = "isSelected";
    }

    @DatabaseField(generatedId = true, columnName = Columns.DREAM_ID)
    private int dreamId;
    @DatabaseField(columnName = Columns.DESCRIPTION)
    private String descriptionDreams;
    @DatabaseField(columnName = Columns.IMAGE_URL)
    private String imageUrl;
    @DatabaseField(columnName = Columns.NAME_DREAM)
    private String nameDream;
    @DatabaseField(columnName = Columns.IS_SELECTED, defaultValue = "false")
    private boolean isSelected;

    public Dream() {
    }

    public int getDreamId() {
        return dreamId;
    }

    public void setDreamId(int dreamId) {
        this.dreamId = dreamId;
    }

    public void setDescriptionDreams(String descriptionDreams) {
        this.descriptionDreams = descriptionDreams;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setNameDream(String nameDream) {
        this.nameDream = nameDream;
    }

    public Dream(String descriptionDreams, String imageUrl, String nameDream) {

        this.descriptionDreams = descriptionDreams;
        this.imageUrl = imageUrl;
        this.nameDream = nameDream;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getDescriptionDreams() {
        return descriptionDreams;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getNameDream() {
        return nameDream;
    }

}