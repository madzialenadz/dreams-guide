package magdalena.pl.dreamguide.model;

public class Emotion {

    private int imageView;
    private int emotionName;

    private boolean isSelected;

    public Emotion(int imageView, int emotionName) {
        this.imageView = imageView;
        this.emotionName = emotionName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getImageView() {
        return imageView;
    }

    public int getEmotionName() {
        return emotionName;
    }

    public void setEmotionName(int emotionName) {
        this.emotionName = emotionName;
    }
}