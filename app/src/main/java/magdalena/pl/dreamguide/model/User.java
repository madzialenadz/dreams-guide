package magdalena.pl.dreamguide.model;

import android.net.Uri;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;

@DatabaseTable(tableName = "User")
public class User {

    public static User fromUser(String idUserString, String fullNameString, String emailString, String photoUrl) {
        User user = new User();
        user.firebaseId = idUserString;
        user.fullName = fullNameString;
        user.email = emailString;
        user.photoString = photoUrl;
        return user;
    }

    public class Columns {
        public static final String USER_ID = "user_id";
        public static final String FIREBASE_ID = "firebase_id";
        public static final String FULLNAME = "fullname";
        public static final String EMAIL = "email";
        public static final String PHOTO_URL = "photo_url";
    }

    public User() {
    }

    public User(String firebaseId, String fullName, String email, String photoString) {
        this.firebaseId = firebaseId;
        this.fullName = fullName;
        this.email = email;
        this.photoString = photoString;
    }

    @DatabaseField(generatedId = true, columnName = Columns.USER_ID)
    private int userId;
    @DatabaseField(columnName = Columns.FIREBASE_ID)
    private String firebaseId;
    @DatabaseField(columnName = Columns.FULLNAME)
    private String fullName;
    @DatabaseField(columnName = Columns.EMAIL)
    private String email;
    @DatabaseField(columnName = Columns.PHOTO_URL)
    private String photoString;

    @ForeignCollectionField(eager = true)
    public ForeignCollection <MyDream> myDreamForeignCollectionField;


    public String getPhotoString() {
        return photoString;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
