package magdalena.pl.dreamguide.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MyDream")
public class MyDream implements Parcelable {

    public MyDream() {
    }

    protected MyDream(Parcel in) {
        myDreamId = in.readInt();
        titleDream = in.readString();
        descriptionDream = in.readString();
        selectDream = in.readString();
        saveDataDream = in.readString();
    }

    public static final Creator<MyDream> CREATOR = new Creator<MyDream>() {
        @Override
        public MyDream createFromParcel(Parcel in) {
            return new MyDream(in);
        }

        @Override
        public MyDream[] newArray(int size) {
            return new MyDream[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(myDreamId);
        dest.writeString(titleDream);
        dest.writeString(descriptionDream);
        dest.writeString(selectDream);
        dest.writeString(saveDataDream);
    }

    public class Columns {
        public static final String MY_DREAM_ID = "my_dream_id";
        static final String USER_ID = "user_id";
        public static final String TITLE_DREAM = "title_dream";
        public static final String DESCRIPTION = "description_dream";
        public static final String SYMBOL_DREAM = "symbol_dream";
        public static final String SELECT_POSITION_EMOTION = "position";
        static final String SAVE_DATA_DREAM = "save_data_dream";
    }

    @DatabaseField(generatedId = true, columnName = Columns.MY_DREAM_ID, uniqueIndex = true)
    private int myDreamId;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, foreignColumnName = Columns.USER_ID)
    private User user;
    @DatabaseField(columnName = Columns.TITLE_DREAM)
    private String titleDream;
    @DatabaseField(columnName = Columns.DESCRIPTION)
    private String descriptionDream;
    @DatabaseField(columnName = Columns.SYMBOL_DREAM)
    private String selectDream;
    @DatabaseField(columnName = MyDream.Columns.SELECT_POSITION_EMOTION)
    private String position;
    @DatabaseField(columnName = MyDream.Columns.SAVE_DATA_DREAM)
    private String saveDataDream;

    public static Creator<MyDream> getCREATOR() {
        return CREATOR;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getMyDreamId() {

        return myDreamId;

    }

    public void setMyDreamId(int myDreamId) {
        this.myDreamId = myDreamId;
    }

    public int setTitleDream(String titleDream) {
        this.titleDream = titleDream;
        return 0;
    }

    public void setDescriptionDream(String descriptionDream) {
        this.descriptionDream = descriptionDream;
    }

    public void setSelectDream(String selectDream) {
        this.selectDream = selectDream;
    }


    public void setSaveDataDream(String saveDataDream) {
        this.saveDataDream = saveDataDream;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MyDream(User user, String titleDream, String descriptionDream, String selectDream, String position, String saveDataDream) {
        this.user = user;
        this.titleDream = titleDream;
        this.descriptionDream = descriptionDream;
        this.selectDream = selectDream;
        this.position = position;
        this.saveDataDream = saveDataDream;
    }

    public MyDream(int myDreamId, String titleDream, String descriptionDream, String selectDream, String position, String saveDataDream) {
        this.myDreamId = myDreamId;
        this.titleDream = titleDream;
        this.descriptionDream = descriptionDream;
        this.selectDream = selectDream;

        this.position = position;
        this.saveDataDream = saveDataDream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyDream myDream = (MyDream) o;

        if (myDreamId != myDream.myDreamId) return false;
        if (position != myDream.position) return false;
        if (user != null ? !user.equals(myDream.user) : myDream.user != null) return false;
        if (titleDream != null ? !titleDream.equals(myDream.titleDream) : myDream.titleDream != null) return false;
        if (descriptionDream != null ? !descriptionDream.equals(myDream.descriptionDream) : myDream.descriptionDream != null) return false;
        if (selectDream != null ? !selectDream.equals(myDream.selectDream) : myDream.selectDream != null) return false;

        return saveDataDream != null ? saveDataDream.equals(myDream.saveDataDream) : myDream.saveDataDream == null;

    }


    public String getTitleDream() {
        return titleDream;
    }

    public String getDescriptionDream() {
        return descriptionDream;
    }

    public String getSelectDream() {
        return selectDream;
    }

    public String getSaveDataDream() {
        return saveDataDream;
    }

}