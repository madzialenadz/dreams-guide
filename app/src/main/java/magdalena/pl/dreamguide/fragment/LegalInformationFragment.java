package magdalena.pl.dreamguide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import magdalena.pl.dreamguide.R;

public class LegalInformationFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View item = inflater.inflate(R.layout.fragment_legal_information, container, false);
        TextView title = (TextView) getActivity().findViewById(R.id.toolbarTextView);
        title.setText(getString(R.string.legal_information));
        return item;
    }
}