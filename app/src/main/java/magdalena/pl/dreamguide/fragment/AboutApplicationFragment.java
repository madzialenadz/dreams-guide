package magdalena.pl.dreamguide.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;

public class AboutApplicationFragment extends Fragment {

    @BindView(R.id.dreamTextView)
    TextView dreamTextView;
    @BindView(R.id.guideTextView)
    TextView guideTextView;
    Unbinder unbinder;
    @BindView(R.id.imageView5)
    ImageView imageView5;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View item = inflater.inflate(R.layout.fragment_application, container, false);
        unbinder = ButterKnife.bind(this, item);

        TextView title = (TextView) getActivity().findViewById(R.id.toolbarTextView);
        title.setText(R.string.aboutApplication);

        return item;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getIntentUrl(String urlString) {
        Uri url = Uri.parse(urlString);
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }

    @OnClick(R.id.privacyPolicyButton)
    public void onViewClicked() {
        getIntentUrl("http://dreamsguide.pl/privacy-policy/");
    }
}