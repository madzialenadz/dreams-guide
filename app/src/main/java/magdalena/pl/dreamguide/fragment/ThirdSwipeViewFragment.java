package magdalena.pl.dreamguide.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;

public class ThirdSwipeViewFragment extends Fragment {


    Unbinder unbinder;

    Handler mHandler = new Handler();
    Runnable mUpdateResults;
    @BindView(R.id.ic_star_one)
    ImageView icStarOne;
    @BindView(R.id.ic_star_seven)
    ImageView icStarSeven;
    @BindView(R.id.ic_star_nine)
    ImageView icStarNine;
    @BindView(R.id.ic_star_five)
    ImageView icStarFive;
    @BindView(R.id.ic_star_three)
    ImageView icStarThree;
    @BindView(R.id.ic_star_two)
    ImageView icStarTwo;
    @BindView(R.id.ic_star_eight)
    ImageView icStarEight;
    @BindView(R.id.ic_star_six)
    ImageView icStarSix;
    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.imageView7)
    ImageView imageView7;
    @BindView(R.id.ic_star_four)
    ImageView icStarFour;

    public ThirdSwipeViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_third_swipe_view, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        LoadUIElements();
        return rootView;
    }

    private void LoadUIElements() {

        int delay = 500; // delay for 1 sec.

        int period = 2000; // repeat every 4 sec.
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(mUpdateResults);
            }
        }, delay, period);

        mUpdateResults = new Runnable() {
            @Override
            public void run() {
                try {
                    AnimationSlideShow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void AnimationSlideShow() {

        try {

            Animation zoom_in = AnimationUtils.loadAnimation(getActivity().getBaseContext().getApplicationContext(), R.anim.zoom_in_out);
            Animation zoom_out = AnimationUtils.loadAnimation(getActivity().getBaseContext().getApplicationContext(), R.anim.zoom_out_in);


        } catch (Exception e) {
            e.printStackTrace();
            ;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}