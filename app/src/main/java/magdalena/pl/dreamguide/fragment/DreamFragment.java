package magdalena.pl.dreamguide.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.adapter.DreamAdapter;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.Dream;



public class DreamFragment extends Fragment implements SearchView.OnQueryTextListener {

    private List<Dream> dreamList;
    private LinearLayoutManager linearLayoutManager;
    private DreamAdapter dreamLoginAdapter;
    private DbHelper dbHelper;
    private Dao<Dream, Integer> dreamDao;
    private Unbinder unbinder;

    @BindView(R.id.recycleDream)
    RecyclerView recycleDream;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dbHelper = new DbHelper(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View item = inflater.inflate(R.layout.fragment_dream, container, false);
        unbinder = ButterKnife.bind(this, item);
        TextView title = (TextView) getActivity().findViewById(R.id.toolbarTextView);
        title.setText(R.string.dreams);
        return item;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            dreamDao = dbHelper.getDao(Dream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            dreamList = dreamDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        int orientation = this.getResources().getConfiguration().orientation;

        if (dreamList != null && dreamList.size() > 0) {

            dreamLoginAdapter = new DreamAdapter(dreamList, getActivity());

            DisplayMetrics metrics = new DisplayMetrics();
            Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay().getMetrics(metrics);

            float yInches = metrics.heightPixels / metrics.ydpi;
            float xInches = metrics.widthPixels / metrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
            if (diagonalInches >= 6.5) {
                // 6.5inch device or bigger
                linearLayoutManager = new GridLayoutManager(getActivity(), 3);
                recycleDream.setLayoutManager(linearLayoutManager);

            } else {
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    linearLayoutManager = new GridLayoutManager(getActivity(), 2);
                    recycleDream.setLayoutManager(linearLayoutManager);
                } else {
                    linearLayoutManager = new GridLayoutManager(getActivity(), 3);
                    recycleDream.setLayoutManager(linearLayoutManager);

                }
            }
            recycleDream.setAdapter(dreamLoginAdapter);
        } else {
            Toast.makeText(getActivity(), "DataBase is null", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search_dream);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_dream));
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.search_dream) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(newText);
        return false;
    }

    void filter(String text) {
        List<Dream> dream = new ArrayList<>();
        for (Dream d : dreamList) {
            if (d.getNameDream().toLowerCase().contains(text)) {
                dream.add(d);
            }
        }
        dreamLoginAdapter.updateList(dream);
    }
}