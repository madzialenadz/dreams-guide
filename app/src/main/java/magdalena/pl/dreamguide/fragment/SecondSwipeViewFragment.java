package magdalena.pl.dreamguide.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;

public class SecondSwipeViewFragment extends Fragment {

    Handler mHandler = new Handler();
    Runnable mUpdateResults;

    Unbinder unbinder;
    @BindView(R.id.ic_pen_view)
    ImageView icPenView;
    @BindView(R.id.ic_diary_view)
    ImageView icDiaryView;

    public SecondSwipeViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_second_swipe_view, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        LoadUIElements();
        return rootView;
    }

    private void LoadUIElements() {

        int delay = 1000; // delay for 1 sec.

        int period = 2000; // repeat every 4 sec.
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(mUpdateResults);
            }
        }, delay, period);

        mUpdateResults = new Runnable() {
            @Override
            public void run() {
                try {
                    AnimationSlideShow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void AnimationSlideShow() {

        try {

            Animation swing= AnimationUtils.loadAnimation(getActivity().getBaseContext().getApplicationContext(), R.anim.swing);

            icPenView.startAnimation(swing);
            icDiaryView.startAnimation(swing);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}