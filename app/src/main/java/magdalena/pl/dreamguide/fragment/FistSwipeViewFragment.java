package magdalena.pl.dreamguide.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;

public class FistSwipeViewFragment extends Fragment {
    @BindView(R.id.moon_image_view)
    ImageView moonImageView;
    @BindView(R.id.star_one)
    ImageView starOne;
    @BindView(R.id.star_two)
    ImageView starTwo;
    @BindView(R.id.star_three)
    ImageView starThree;
    Unbinder unbinder;

    Handler mHandler = new Handler();
    Runnable mUpdateResults;

    public FistSwipeViewFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_first_swipe_view, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        LoadUIElements();
        return rootView;
    }

    private void LoadUIElements() {

        int delay = 1000; // delay for 1 sec.

        int period = 2000; // repeat every 4 sec.
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(mUpdateResults);
            }
        },delay,period);

        mUpdateResults = new Runnable() {
            @Override
            public void run() {
                try {
                    AnimationSlideShow();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }

    private void AnimationSlideShow() {
        try {
            Animation leftRote = AnimationUtils.loadAnimation(getActivity().getBaseContext().getApplicationContext(),R.anim.rotate_left);
            Animation rightRote = AnimationUtils.loadAnimation(getActivity().getBaseContext().getApplicationContext(),R.anim.rotate_right);
            starOne.startAnimation(leftRote);
            starTwo.startAnimation(rightRote);
            starThree.startAnimation(leftRote);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}