package magdalena.pl.dreamguide.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.R;

public class ContactFragment extends Fragment {

    @BindView(R.id.ImageView5)
    ImageView ImageView5;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        TextView title = (TextView) getActivity().findViewById(R.id.toolbarTextView);
        title.setText(R.string.contact);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.websiteTextView, R.id.facebookLink, R.id.twitterLink, R.id.linkedinLink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.websiteTextView:
                getIntentUrl("http://www.magdalenadziesinska.pl/");
                break;
            case R.id.facebookLink:
                getIntentUrl("https://web.facebook.com/magdalena.dziesinska");
                break;
            case R.id.twitterLink:
                getIntentUrl("https://twitter.com/DziesiskaMadzia");
                break;
            case R.id.linkedinLink:
                getIntentUrl("https://www.linkedin.com/in/magdalena-dziesi%C5%84ska-3b15b194/");
                break;
        }
    }

    private void getIntentUrl(String urlString) {
        Uri url = Uri.parse(urlString);
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }
}