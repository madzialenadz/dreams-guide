package magdalena.pl.dreamguide.fragment;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import magdalena.pl.dreamguide.MySharePreference;
import magdalena.pl.dreamguide.R;
import magdalena.pl.dreamguide.db.DbHelper;
import magdalena.pl.dreamguide.model.MyDream;
import magdalena.pl.dreamguide.model.User;

public class StatisticFragment extends Fragment {

    @BindView(R.id.pieChart)
    PieChart pieChart;
    private Unbinder unbinder;
    private Dao<MyDream, Integer> myDreamDao;
    private Dao<User, Integer> myUserDao;
    private DbHelper dbHelper;
    private long none, sad, scared, angry, happy = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View item = inflater.inflate(R.layout.fragment_statistic, container, false);
        unbinder = ButterKnife.bind(this, item);
        TextView title = (TextView) getActivity().findViewById(R.id.toolbarTextView);
        title.setText(R.string.title_activity_statistics);
        ButterKnife.bind(this, item);
        return item;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MySharePreference mySharePreference = new MySharePreference();
        String userIdString = mySharePreference.getFirebaseId(getActivity());

        dbHelper = new DbHelper(getActivity());

        try {
            myDreamDao = dbHelper.getDao(MyDream.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            myUserDao = dbHelper.getDao(User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        QueryBuilder<User, Integer> userQa = myUserDao.queryBuilder();

        try {
            userQa.where().eq(User.Columns.FIREBASE_ID, userIdString);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        QueryBuilder<MyDream, Integer> myDreamQa = myDreamDao.queryBuilder();

        try {
            myDreamQa.join(userQa).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            happy = myDreamQa.where().eq(MyDream.Columns.SELECT_POSITION_EMOTION, "0").countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            none = myDreamQa.where().eq(MyDream.Columns.SELECT_POSITION_EMOTION, "1").countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            scared = myDreamQa.where().eq(MyDream.Columns.SELECT_POSITION_EMOTION, "2").countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            sad = myDreamQa.where().eq(MyDream.Columns.SELECT_POSITION_EMOTION, "3").countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            angry = myDreamQa.where().eq(MyDream.Columns.SELECT_POSITION_EMOTION, "4").countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        float sadFloat = sad;
        float noneFloat = none;
        float scaredFloat = scared;
        float angryFloat = angry;
        float happyFloat = happy;
        float sum = sadFloat + noneFloat + scaredFloat + angryFloat + happyFloat;
        float sadT = (sadFloat / sum) * 100;
        float noneT = (noneFloat / sum) * 100;
        float scaredT = (scaredFloat / sum) * 100;
        float angryT = (angryFloat / sum) * 100;
        float happyT = (happyFloat / sum) * 100;

        if (sadFloat == 0.0 && noneFloat == 0.0 && scaredFloat == 0.0 && angryFloat == 0.0 && happyFloat == 0.0) {

            pieChart.setNoDataText(getString(R.string.no_dream_add_yet));
            Paint p = pieChart.getPaint(Chart.PAINT_INFO);
            p.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserrat_regular.ttf"));
            p.setColor(Color.WHITE);
            p.setTextSize(45f);
        } else {

            pieChart.setEntryLabelColor(Color.BLACK);
            pieChart.setEntryLabelTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserrat_regular.ttf"));
            pieChart.setEntryLabelTextSize(13f);

            List<PieEntry> values = new ArrayList<>();
            values.add(new PieEntry(sadT, R.string.sad));
            values.add(new PieEntry(noneT, R.string.none));
            values.add(new PieEntry(scaredT, R.string.scared));
            values.add(new PieEntry(angryT, R.string.angry));
            values.add(new PieEntry(happyT, R.string.happy));

            Legend legend = pieChart.getLegend();
            legend.setEnabled(false);

            PieDataSet dataSet = new PieDataSet(values, "");
            dataSet.setColors(Color.parseColor("#95A5A6"), Color.parseColor("#ECF0F1"), Color.parseColor("#2980B9"), Color.parseColor("#E74C3C"), Color.parseColor("#F1C40F"));

            PieData data = new PieData(dataSet);
            data.setDrawValues(true);
            data.setValueFormatter(new PercentFormatter());
            data.setValueTextColor(Color.BLACK);
            data.setValueTextSize(13f);
            data.setValueTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserrat_regular.ttf"));
            pieChart.setData(data);
            pieChart.invalidate();
            pieChart.getDescription().setEnabled(false);
            pieChart.setDrawHoleEnabled(false);
            pieChart.animateXY(1400, 1400);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
