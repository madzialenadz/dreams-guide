package magdalena.pl.dreamguide;

import android.app.Activity;
import android.content.Intent;

public class ThemeUnits {

    private static int Theme;

    private final static int DEFAULT = 0;

    private final static int RED = 1;

    private final static int PINK = 2;
    
    public static void changeToTheme(Activity activity, int theme) {
        Theme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    public static void onActivityCreateSetTheme(Activity activity) {
        switch (Theme)
        {
            case DEFAULT:
                break;
            case RED:
                break;
            case PINK:
                break;
        }
    }
}
